package com.ilabservice.darwinrecorder.hlslive.entity;

import java.util.List;
import java.util.Map;

public class Stat {

    private int ClusterSize;
    private int StreamingCount;
    private int DecodingCount;
    private int RecordingCount;
    private int MaxBroadcasting;
    private int MaxStreamingCount;
    private int MaxDecodingCount;
    private int MaxRecordingCount;
    private int MaxHistoryBroadcasting;
    private int TotalRecords;
    private int TotalEvents;
    private double TotalRecordingHours;

    public List<FailureCounts> getFailureCounts() {
        return failureCounts;
    }

    public void setFailureCounts(List<FailureCounts> failureCounts) {
        this.failureCounts = failureCounts;
    }

    public List<TopKFailureStreaming> getTopKFailureStreaming() {
        return topKFailureStreaming;
    }

    public void setTopKFailureStreaming(List<TopKFailureStreaming> topKFailureStreaming) {
        this.topKFailureStreaming = topKFailureStreaming;
    }

    private List<FailureCounts> failureCounts;
    private List<TopKFailureStreaming> topKFailureStreaming;




    public int getClusterSize() {
        return ClusterSize;
    }

    public void setClusterSize(int clusterSize) {
        ClusterSize = clusterSize;
    }

    public int getStreamingCount() {
        return StreamingCount;
    }

    public void setStreamingCount(int streamingCount) {
        StreamingCount = streamingCount;
    }

    public int getDecodingCount() {
        return DecodingCount;
    }

    public void setDecodingCount(int decodingCount) {
        DecodingCount = decodingCount;
    }

    public int getRecordingCount() {
        return RecordingCount;
    }

    public void setRecordingCount(int recordingCount) {
        RecordingCount = recordingCount;
    }

    public int getMaxBroadcasting() {
        return MaxBroadcasting;
    }

    public void setMaxBroadcasting(int maxBroadcasting) {
        MaxBroadcasting = maxBroadcasting;
    }

    public int getMaxStreamingCount() {
        return MaxStreamingCount;
    }

    public void setMaxStreamingCount(int maxStreamingCount) {
        MaxStreamingCount = maxStreamingCount;
    }

    public int getMaxDecodingCount() {
        return MaxDecodingCount;
    }

    public void setMaxDecodingCount(int maxDecodingCount) {
        MaxDecodingCount = maxDecodingCount;
    }

    public int getMaxRecordingCount() {
        return MaxRecordingCount;
    }

    public void setMaxRecordingCount(int maxRecordingCount) {
        MaxRecordingCount = maxRecordingCount;
    }

    public int getMaxHistoryBroadcasting() {
        return MaxHistoryBroadcasting;
    }

    public void setMaxHistoryBroadcasting(int maxHistoryBroadcasting) {
        MaxHistoryBroadcasting = maxHistoryBroadcasting;
    }

    public int getTotalRecords() {
        return TotalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        TotalRecords = totalRecords;
    }

    public int getTotalEvents() {
        return TotalEvents;
    }

    public void setTotalEvents(int totalEvents) {
        TotalEvents = totalEvents;
    }

    public double getTotalRecordingHours() {
        return TotalRecordingHours;
    }


    public void setTotalRecordingHours(double totalRecordingHours) {
        TotalRecordingHours = totalRecordingHours;
    }


}
