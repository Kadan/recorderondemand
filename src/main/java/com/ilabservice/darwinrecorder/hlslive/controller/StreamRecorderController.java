package com.ilabservice.darwinrecorder.hlslive.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.io.Files;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.config.MQTTConfiguration;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.cron.DarwinListener;
import com.ilabservice.darwinrecorder.hlslive.entity.MQTTEnvelope;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.ErrorMeasurement;
import com.ilabservice.darwinrecorder.hlslive.measurements.RecorderMeasurement;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.sound.midi.SysexMessage;
import java.io.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class StreamRecorderController {

    @Autowired
    Map<String, String> recordingList;

    @Autowired
    Handler handler;

    @Autowired
    FFMPEGUtils ffmpegUtils;
    @Value("${darwinPrefix}")
    private String darwinPrefix;

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;

    @Value("${mqtt.producer.sendMessage}")
    private boolean sendMsg;

    @Autowired
    DarwinListener darwinListener;
    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    List<String> allStreams;

    @Autowired
    List<String> allRTSP;

    @Autowired
    List<String> allRTSPInProgress;

    @Autowired
    private MQTTConfiguration.MqttGateway mqttGateway;

//    @Autowired
//    Map<String, Long> requestMap;
//
//    @Autowired
//    Map<String, String> stoppingMap;

    @Autowired
    VideoService videoService;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    //    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final Logger log = LoggerFactory.getLogger(StreamRecorderController.class);

    @GetMapping("/video/remove/{stream}")
    public ResponseEntity removeRecorder(HttpServletRequest request, @PathVariable String stream){
        log.info("Stop recording stream " + stream + " forcibilly");
        RecorderMeasurement rm = new RecorderMeasurement();
        rm.setTime(System.currentTimeMillis());
        rm.setEvent("Force the stream to stop");
        rm.setType("videoRecorder");
        rm.setName(stream);
        rm.setRecordingCount(1);
        rm.setRecordedHours(0);
        rm.setRecords(0);
        Point point = Point.measurementByPOJO(rm.getClass()).addFieldsFromPOJO(rm).build();
        influxDbUtils.influxDB.write(point);
        recorderRedisHandler.removeRecorderStatus(stream);
        recorderRedisHandler.removeStoppingStatus(stream);
        return new ResponseEntity<String>("The stream  " + stream + " is been removed from recording", HttpStatus.OK);
    }


    @GetMapping("/video/recorder")
    public List<String> getRecordingList(){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String, String> rtmp = recorderRedisHandler.getRTMPRecorderMap();
        if(requestMap != null){
            List<String> rtmpList = rtmp.keySet().stream().collect(Collectors.toList());
            List<String> rtsp = requestMap.keySet().stream().collect(Collectors.toList());
            List<String> retList = new ArrayList<String>();
            if(rtmpList!=null)
                retList.addAll(rtmpList);
            if(rtsp != null)
                retList.addAll(rtsp);

            return retList;
        }
        else
            return new ArrayList<>();

    }
    //    @Scheduled(cron = "0/5 * * * * ?")
    public void daemon(){
        log.info("prepare all streams ...");
        allStreams = darwinListener.getAllStream();
        allStreams.add("rtmp://live.chosun.gscdn.com/live/tvchosun1.stream");
//        passiveList.stream().filter(s->
//            allStreams.contains(s)
//        ).forEach(s->{
//            log.info("the stream exists in passivelist, now checking in" + s);
//            record(s);
//            passiveList.remove(s);
//        });
    }

    @Scheduled(cron = "0 0/3 * * * ? ")
    private void removeUnnessaryRawReady(){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        long retention = 1 * 60 * 60 * 1000L;
        List<String> fetchList = darwinListener.getAllRTSP();
        fetchList.stream().forEach(s->{
            String recordPath = videoRecordPath_temp + File.separator +
                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;
            File folder = new File(recordPath);
            if(requestMap.entrySet().size()==0 || !requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
                new Thread(() ->{
                    //remove unnecessary raw-ready
                    //String currentFile = ffmpegUtils.currentFile(recordPath);
                    if(folder !=null && folder.isDirectory()){
                        Arrays.stream(folder.listFiles()).filter(sss->sss.isFile()&&sss.getName().endsWith("mp4")
                                //&& !sss.getName().equalsIgnoreCase(currentFile)
                                && sss.getName().contains("raw-ready")).forEach(sss->{
                            log.info(s + "No record request ... Removing unnecessary segmentation file " + sss.getName());
                            long created = Long.valueOf(StringUtils.substringBetween(sss.getName(),"raw-ready",".mp4"));
                            long last_s = created + retention;
                            if(last_s<System.currentTimeMillis())
                                sss.delete();
                        });
                    }

                }).start();
            }
        });
    }

    @Scheduled(cron = "0/10 * * * * ?")
    public void ring(){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String,String> stoppingMap = recorderRedisHandler.getStoppingMap();
        log.info("ring recording ...");
        List<String> fetchList = darwinListener.getAllRTSP();
//        List<String> fetchList = new ArrayList<>();
//        fetchList.add("rtsp://qz.videostreaming.ilabservice.cloud:554/D72158832");

        fetchList.stream().filter(s-> !stoppingMap.keySet().contains(StringUtils.substringAfterLast(s,"/")) &&
                !handler.streamLive(StringUtils.substringAfterLast(s,"/"))).forEach(s->{
            //&& !allRTSPInProgress.contains(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"))

            log.info("ring recording for " + s);
            if (!videoRecordPath_temp.endsWith(File.separator))
                videoRecordPath_temp = videoRecordPath_temp + File.separator;
            String recordPath = videoRecordPath_temp +
                    org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/") + "-manual" + File.separator;

            File folder = new File(recordPath);

            if(!folder.exists())
                folder.mkdirs();


            //If the stream is not being recorded,then remove the raw ready segmentation
            if(requestMap.entrySet().size()==0||!requestMap.keySet().stream().anyMatch(ss->ss.equalsIgnoreCase(org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/")))) {
                new Thread(() -> {
                    if (requestMap.entrySet().size() == 0) {
                        if (folder != null && folder.isDirectory()) {
                            Arrays.stream(folder.listFiles()).filter(sss -> sss.isFile() && sss.getName().endsWith("mp4")
                                    && sss.getName().contains("raw-ready")).forEach(sss -> {
                                log.info("Removing unnecessary segmentation file>>>" + sss.getName());
                                sss.delete();
                            });
                        }
                    }
                }).start();
            }


            String file = recordPath;
            log.info("ring recording for " + recordPath +  "   >>> " +  file);
            ffmpegUtils.recordRaw(s,file,org.apache.commons.lang3.StringUtils.substringAfterLast(s,"/"));

        });
    }
    @ApiOperation("开始录像")
    @ApiResponses({ @ApiResponse(code = 200, message = "开始录像成功"),
            @ApiResponse(code = 400, message = "开始录像失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "stream", value = "D70002092", required = true)
    })
    @GetMapping("/video/recordStart/{stream}")
    public ResponseEntity<String> markRecord(HttpServletRequest request, @PathVariable String stream){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        if(requestMap.keySet().stream().anyMatch(s->s.contains(stream))){
            return new ResponseEntity<String>("The stream  " + stream + " is already being recorded", HttpStatus.BAD_REQUEST);
        }
        String recordPath = videoRecordPath_temp + File.separator +
                stream + "-manual" + File.separator;
        String currentFile = ffmpegUtils.currentFile(recordPath);

        List<String> fetchList = darwinListener.getAllRTSP();
        if(!fetchList.stream().anyMatch(s->s.contains(stream))){
            log.info("The stream is yet to live in darwin will revoke  >>>> " + stream);
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("videoRecorder");
            em.setReason("stream is not live on darwin");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
            recorderRedisHandler.saveRecorderStatus(stream,System.currentTimeMillis());
            if(new File(recordPath).exists())
                Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
                        !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
                    log.info("removing unnessary raw-ready file " + s.getName());
                    s.delete();
                });
            return new ResponseEntity<String>("The stream is yet to live in darwin will revoke " + stream, HttpStatus.BAD_REQUEST);
        }


        Arrays.stream(new File(recordPath).listFiles()).filter(s->s.isFile() && s.getName().startsWith("raw-ready") && s.getName().endsWith(".mp4") &&
                !StringUtils.equalsIgnoreCase(s.getName(),currentFile)).forEach(s->{
            log.info("removing unnessary raw-ready file " + s.getName());
            s.delete();
        });

        recorderRedisHandler.saveRecorderStatus(stream,System.currentTimeMillis());

        RecorderMeasurement rm1 = new RecorderMeasurement();
        rm1.setTime(System.currentTimeMillis());
        rm1.setEvent("start to record stream");
        rm1.setType("videoRecorder");
        rm1.setName(stream);
        rm1.setRecordingCount(requestMap.keySet().size());
        rm1.setRecordedHours(0);
        rm1.setRecords(0);
        Point point = Point.measurementByPOJO(rm1.getClass()).addFieldsFromPOJO(rm1).build();
        influxDbUtils.influxDB.write(point);
        return new ResponseEntity<String>("The stream" + stream + " is being recorded", HttpStatus.OK);
    }


    @ApiOperation("停止录像")
    @ApiResponses({ @ApiResponse(code = 200, message = "停止录像成功"),
            @ApiResponse(code = 400, message = "停止录像失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "stream", value = "D70002092", required = true)
    })
    @GetMapping("/video/recordStop/{stream}")
    public ResponseEntity markStop(HttpServletRequest request, @PathVariable String stream) {
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();
        Map<String, String> stoppingMap = recorderRedisHandler.getStoppingMap();
        if (!requestMap.keySet().contains(stream)) {
            return new ResponseEntity<String>("The stream" + stream + " has already been stopped", HttpStatus.OK);
        }

        if (stoppingMap.keySet().contains(stream)) {
            return new ResponseEntity<String>("The stream" + stream + " is in progress of stopping ...", HttpStatus.BAD_REQUEST);
        }

        if (!videoRecordPath_temp.endsWith(File.separator))
            videoRecordPath_temp = videoRecordPath_temp + File.separator;


        if (!videoRecordPath.endsWith(File.separator))
            videoRecordPath = videoRecordPath + File.separator;

        String recordPath_temp = videoRecordPath_temp +
                stream + "-manual" + File.separator;

        String recordPath = videoRecordPath +
                stream + "-manual" + File.separator;


        if (!new File(recordPath_temp).exists())
            new File(recordPath_temp).mkdirs();



        RecorderMeasurement rm1 = new RecorderMeasurement();
        rm1.setTime(System.currentTimeMillis());
        rm1.setEvent("Start to stop record stream");
        rm1.setType("videoRecorder");
        rm1.setName(stream);
        rm1.setRecordingCount(1);
        rm1.setRecordedHours(0);
        rm1.setRecords(0);
        Point point1 = Point.measurementByPOJO(rm1.getClass()).addFieldsFromPOJO(rm1).build();
        influxDbUtils.influxDB.write(point1);

        Long statTime = requestMap.get(stream);
        Long endTime = System.currentTimeMillis();
        DecimalFormat df = new DecimalFormat("#.0");
        try {
            handler.Stop(stream);
            //let the file to be written properlly...
        } catch (Exception e) {
            e.printStackTrace();
            ErrorMeasurement em = new ErrorMeasurement();
            em.setTime(System.currentTimeMillis());
            em.setType("videoRecorder");
            em.setReason("unable to stop record the stream");
            em.setName(stream);
            Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
            influxDbUtils.influxDB.write(point);
//            recorderRedisHandler.removeStoppingStatus(stream);
            recorderRedisHandler.removeRecorderStatus(stream);
            return new ResponseEntity<String>("The recording for " + stream + "  has been stopped, however, could not save video ", HttpStatus.BAD_REQUEST);
        }

        if (statTime == null || statTime < 0) {
//            recorderRedisHandler.removeStoppingStatus(stream);
            recorderRedisHandler.removeRecorderStatus(stream);
            log.info("The stream " + stream + " has not startted yet, but now stopped  ");
            return new ResponseEntity<String>("The stream " + stream + " has not startted yet, but now stopped  ", HttpStatus.BAD_REQUEST);
        }

        new Thread(() -> {
            if (new File(recordPath_temp).exists()) {
                if (!new File(recordPath).exists())
                    new File(recordPath).mkdirs();
                String subFolder = recordPath + statTime + "-" + endTime + File.separator;
                String downloadPath = fileDownloadPath.endsWith(File.separator) ? fileDownloadPath + stream + "-manual" + File.separator + statTime + "-" + endTime + File.separator
                        : fileDownloadPath + File.separator + stream + "-manual" + File.separator + statTime + "-" + endTime + File.separator;

                //Copy file into the destination folder
                List<File> readyfiles = Arrays.asList(new File(recordPath_temp).listFiles());

                if (readyfiles!=null & readyfiles.size()>0 && !new File(subFolder).exists()){
                    new File(subFolder).mkdirs();
                }
//                try{
//                    Thread.sleep(1400);}catch(Exception e){
//                }
                Map<String, String> stoppingMap_2 = recorderRedisHandler.getStoppingMap();
                if(!stoppingMap_2.keySet().contains(stream))
                    recorderRedisHandler.saveStoppingStatus(stream);
                else{
                    //other thread is handling ....
                    recorderRedisHandler.removeRecorderStatus(stream);
                    recorderRedisHandler.removeRecorderStatus(stream);
                    log.info("other thread concurrently handling stopping... ...");
                    return;
                }
                String massageFolder = subFolder;
                Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                        && s.getName().contains("raw-ready")).forEach(s -> {
                    try {
                        log.info("copying file " + s.getName() + " to " + massageFolder + File.separator);
                        Files.copy(s, new File(massageFolder + File.separator + s.getName()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        ErrorMeasurement em = new ErrorMeasurement();
                        em.setTime(System.currentTimeMillis());
                        em.setType("videoRecorder");
                        em.setReason("can not copy file to destination");
                        em.setName(recordPath_temp + File.separator + s.getName());
                        Point point = Point.measurementByPOJO(ErrorMeasurement.class).addFieldsFromPOJO(em).build();
                        influxDbUtils.influxDB.write(point);
                    }
                });

                if(!new File(massageFolder).exists() || !new File(subFolder).exists()){
                    recorderRedisHandler.removeRecorderStatus(stream);
                    recorderRedisHandler.removeRecorderStatus(stream);
                    log.info("no files in the targer folder , stop ...");
                    return;
                }


                //check whether there are raw files in the destination folder;
                List<File> recordedFiles = Arrays.stream(new File(massageFolder).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                        && s.getName().contains("raw-ready")).collect(Collectors.toList());
                if(recordedFiles!=null && recordedFiles.size()>0){
                    //handle the raw-readys
                    List<String> vaildFiles = new ArrayList<String>();
                    boolean indexing = false;
                    PrintWriter out = null;
                    try{
                        String index = massageFolder + "index.txt";

                        if(new File(massageFolder + "index.txt").exists())
                            new File(massageFolder + "index.txt").delete();

                        out = new PrintWriter(new BufferedWriter(
                                new FileWriter(index, true)));
                        int totalDuration = 0;

                        for (int i = 0; i < recordedFiles.size(); i++) {
                            String fName = recordedFiles.get(i).getName();
                            if(ffmpegUtils.validVideo(massageFolder+fName)){//only writes the valid files which is fbs > 50
                                int subDuration = ffmpegUtils.getVideoTime(massageFolder+fName);
                                if(subDuration<2){
                                    continue;
                                }
                                totalDuration = totalDuration + subDuration;
                                out.println("file " + fName);
                                vaildFiles.add(fName);
                                indexing=true;
                            }
                            else
                                continue;
                        }

                        if(!indexing){
                            recorderRedisHandler.removeRecorderStatus(stream);
                            recorderRedisHandler.removeRecorderStatus(stream);
                            log.info("could not found any valid file to be concat");
                            return;
                        }else{
                            if (out != null)
                                out.close();
                        }

                        if(vaildFiles.size() <=0){
                            recorderRedisHandler.removeRecorderStatus(stream);
                            recorderRedisHandler.removeRecorderStatus(stream);
                            log.info("could not found any valid files");
                            return;
                        }

                        Collections.sort(vaildFiles);

                        String outputFile = subFolder + statTime + "-" + endTime + ".mp4";
                        String outputImg = subFolder + + statTime + "-" + endTime + ".jpg";
                        String outputFile_dl = downloadPath + statTime + "-" + endTime + ".mp4";
                        String outputImg_dl = downloadPath + + statTime + "-" + endTime + ".jpg";


                        int startOffSet = 0;
                        int duration = 0;

                        long firstCreated = Long.valueOf(StringUtils.substringBetween(vaildFiles.get(0),"raw-ready", ".mp4"));
                        long lastModif = Long.valueOf(StringUtils.substringBetween(vaildFiles.get(vaildFiles.size()-1),"raw-ready", ".mp4"));

                        int gap = getDuration(firstCreated,statTime);
                        if(gap>0){
                            startOffSet = gap;
                            duration = totalDuration - startOffSet;
                        }else{
                            duration = totalDuration;
                        }

                        try{
                            ffmpegUtils.concatWithStartoffsetAndDuration(index, outputFile, startOffSet, duration, outputImg);
                            log.info(" concat finished .... ");
                            Video v = new Video();
                            v.setStartTime(statTime);
                            v.setEndTime(lastModif);
                            v.setCameraId(stream);
                            v.setVideo(outputFile_dl);
                            v.setImage(outputImg_dl);
                            v.setLength(Long.valueOf(duration));
                            v.setStatus(Constants.CREATED);
                            v.setSize(new File(outputFile).length());
                            v.setVaStartTime(System.currentTimeMillis());
                            videoService.save(v);
                            //remove the raw files
                            Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                                    && s.getName().contains("raw-ready")).forEach(s -> s.delete());

                            recorderRedisHandler.removeStoppingStatus(stream);
                            recorderRedisHandler.removeRecorderStatus(stream);

                            if (sendMsg) {
                                mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(v));
                            }

                        }catch(Exception e){
                            recorderRedisHandler.removeStoppingStatus(stream);
                            recorderRedisHandler.removeRecorderStatus(stream);
                            log.info("Could not concat the raw files ... ");
                            return;
                        }

                    }catch(Exception e){
                        recorderRedisHandler.removeStoppingStatus(stream);
                        recorderRedisHandler.removeRecorderStatus(stream);
                        log.info("Exception during handling destination files");
                        return;
                    }finally {
                        if (out != null)
                            out.close();
                    }
                }else{
                    recorderRedisHandler.removeStoppingStatus(stream);
                    log.info("there is no files in destination folder " + massageFolder);
                    recorderRedisHandler.removeRecorderStatus(stream);
                    return;
                }
            }else{ // if there is no record sub folder, return
                recorderRedisHandler.removeStoppingStatus(stream);
                log.info("there is no such a folder   " + recordPath_temp);
                recorderRedisHandler.removeRecorderStatus(stream);
                return;
            }

        }).start();

        return new ResponseEntity("Recording has been stopped for " + stream, HttpStatus.OK);
    }


    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
    }

}
