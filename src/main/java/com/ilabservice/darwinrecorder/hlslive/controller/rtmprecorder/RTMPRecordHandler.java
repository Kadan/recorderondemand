package com.ilabservice.darwinrecorder.hlslive.controller.rtmprecorder;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@RestController
public class RTMPRecordHandler {

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    private static final String RTMP_SUFIX = "-rtmp";

    @Autowired
    FFMPEGUtils ffmpegUtils;

    @Autowired
    Handler handler;

    @Autowired
    VideoService videoService;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;


    @Value("${rtmpRecordSaveMax}")
    private int rtmpRecordSaveMax;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    private static final Logger log = LoggerFactory.getLogger(RTMPRecordHandler.class);
    @ApiOperation("保存rtmp record")
    @ApiResponses({ @ApiResponse(code = 200, message = "the video is being saved, please wait for a moment in order to complete the video saving progress"),
            @ApiResponse(code = 500, message = "保存rtmp record出错"),
            @ApiResponse(code = 404, message = "无对应时段录像")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "path", dataType = "String", name = "sn", value = "D70008503", required = true),
            @ApiImplicitParam(paramType = "path", dataType = "long", name = "timestamp", value = "1580631032120", required = true),
            @ApiImplicitParam(paramType = "path", dataType = "int", name = "len", value = "600", required = true)
    })
    @PostMapping("/save/recording/camera/{sn}/start/{timestamp}/length/{len}")
    public ResponseEntity saveRTMPRecord(HttpServletRequest request, @PathVariable String sn
            , @PathVariable long timestamp,@PathVariable int len) {

        Map<String,String> rtmpRecorderMap = recorderRedisHandler.getRTMPRecorderMap();

        if(!rtmpRecorderMap.keySet().contains(sn+RTMP_SUFIX))
            return new ResponseEntity("The stream " + sn + " is not being recorded .", HttpStatus.NOT_FOUND);

        if(rtmpRecordSaveMax<=0)
            rtmpRecordSaveMax=30;
        if(len > rtmpRecordSaveMax*60)
            return new ResponseEntity("The maximum record length can be saved is " + rtmpRecordSaveMax + " minutes", HttpStatus.BAD_REQUEST);

        if(timestamp >= System.currentTimeMillis())
            return new ResponseEntity("Requested start time should be earlier than now()", HttpStatus.BAD_REQUEST);

        String rtmpUrl = rtmpRecorderMap.get(sn+RTMP_SUFIX);

        if(!videoRecordPath_temp.endsWith(File.separator)) videoRecordPath_temp = videoRecordPath_temp + File.separator;
        //probe into the temp path for the sn
        String recordPath = videoRecordPath_temp +
                sn + RTMP_SUFIX + File.separator;
        if(!videoRecordPath.endsWith(File.separator)) videoRecordPath = videoRecordPath + File.separator;
        if(!fileDownloadPath.endsWith(File.separator))fileDownloadPath=fileDownloadPath+ File.separator;

        String savePath = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L);
        String outputFile = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator + timestamp + "-" + (timestamp+len*1000L) + ".mp4";
        String outputImg = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator + timestamp + "-" + (timestamp+len*1000L) + ".jpg";

        String index = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator + "index.txt";

        String movedFilePre = videoRecordPath + sn + RTMP_SUFIX + File.separator + timestamp + "-" + (timestamp+len*1000L) +
                File.separator;

        if(new File(movedFilePre).exists())
            return new ResponseEntity(" Record already exists for the stream " + sn + " with the start timestamp " + timestamp + " and given length " + len, HttpStatus.BAD_REQUEST);

        if(new File(recordPath).exists()) {
            File[] rawFiles = new File(recordPath).listFiles();
            if (rawFiles != null && rawFiles.length > 0) {


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        long now = System.currentTimeMillis();
                        if(timestamp + len*1000 + 200000 >= now){
                            try {
                                Thread.sleep(timestamp + len*1000 + 200000 - now  );
                                log.info("wakeup and start to handle the job...");
                            } catch (InterruptedException e) {
                            }
                        }
                        File[] rawFiles = new File(recordPath).listFiles();
                        List<File> readyFiles = Arrays.stream(rawFiles).filter(f -> StringUtils.containsIgnoreCase(f.getName(), "raw-ready")
                                && StringUtils.containsIgnoreCase(f.getName(), ".mp4")).collect(Collectors.toList());
                        //sort the files
                        Collections.sort(readyFiles);
                        //the first file's creation time
                        long firstCreated = Long.valueOf(StringUtils.substringBetween(readyFiles.get(0).getName(), "raw-ready", ".mp4"));
                        long lastStarted = Long.valueOf(StringUtils.substringBetween(readyFiles.get(readyFiles.size()-1).getName(), "raw-ready", ".mp4"));

                        int startOffSet = 0;
                        int duration = 0;
                        int totalDuration = 0;
                        //recorderRedisHandler.saveStoppingStatus(sn + RTMP_SUFIX);
                        //check the required end time
                        long requiredEnd = timestamp + len * 1000L;
                        long lastFileStart = Long.valueOf(StringUtils.substringBetween(readyFiles.get(readyFiles.size() - 1).getName(), "raw-ready", ".mp4"));
                        //delete the previous index file
                        if (new File(index).exists())
                            new File(index).delete();
                        PrintWriter out = null;
                        try {

                            if (!new File(savePath).exists()) new File(savePath).mkdirs();
                            out = new PrintWriter(new BufferedWriter(
                                    new FileWriter(index, true)));
                            List<String> vaildList = new ArrayList<String>();
                            boolean indexing = false;
                            for (int i = 0; i < readyFiles.size(); i++) {
                                String fName = readyFiles.get(i).getName();
                                long created = Long.valueOf(StringUtils.substringBetween(fName,"raw-ready",".mp4"));
                                long lastM = readyFiles.get(i).lastModified();
                                if (isFileInScopeV2(created,timestamp,requiredEnd,created+200*1000L)) {//only writes the valid files which is fbs > 50, and total duration still in scope
                                    if(!ffmpegUtils.validVideo(recordPath + fName) && ffmpegUtils.getVideoTime(recordPath + fName)>2)
                                        continue;
                                    out.println("file " + fName);
                                    FileCopyUtils.copy(new File(recordPath + fName),new File(movedFilePre+fName));
                                    totalDuration = totalDuration + ffmpegUtils.getVideoTime(movedFilePre+fName);
                                    vaildList.add(fName);
                                    indexing=true;
                                    //getDuration(Long.valueOf(StringUtils.substringBetween(readyFiles.get(i).getName(),"raw-ready",".mp4")),readyFiles.get(i).lastModified());
                                } else
                                    continue;
                            }

                            if (out != null)
                                out.close();

                            if(!indexing){
                                log.info("could not found any valid file to be concat");
                                //recorderRedisHandler.removeStoppingStatus(sn + RTMP_SUFIX);
                                return;
                            }

                            Collections.sort(vaildList);
                            if(vaildList.size()>0){
                                long firstCreatedValid = Long.valueOf(StringUtils.substringBetween(vaildList.get(0),"raw-ready",".mp4"));
                                if (timestamp > firstCreatedValid)
                                    startOffSet = getDuration(firstCreatedValid, timestamp);
                            }

                            if (len > totalDuration)
                                duration = totalDuration;
                            else
                                duration = len;



                            try{
                                ffmpegUtils.concatWithStartoffsetAndDuration(index, outputFile, startOffSet, duration, outputImg);
                            }catch(Exception e){
                                //recorderRedisHandler.removeStoppingStatus(sn + RTMP_SUFIX);
                                return;
                            }
                            //recorderRedisHandler.removeStoppingStatus(sn + RTMP_SUFIX);
                            Video v = new Video();
                            v.setSize(new File(outputFile).length());
                            v.setCameraId(sn);
                            v.setVideo(StringUtils.replace(outputFile, videoRecordPath, fileDownloadPath));
                            v.setImage(StringUtils.replace(outputImg, videoRecordPath, fileDownloadPath));
                            v.setStartTime(timestamp);
                            v.setEndTime((timestamp + len * 1000L));
                            v.setStatus(Constants.CREATED);
                            v.setLength(Long.valueOf(duration));
                            videoService.save(v);

                        } catch (IOException e) {
                            e.printStackTrace();
                            log.info("could not prepare the index file " + savePath + "index.txt");
                            if(out!=null)
                                out.close();
                            //recorderRedisHandler.removeStoppingStatus(sn + RTMP_SUFIX);
                        }

                    }
                }
                ).start();

//                new Thread(() -> ).start();

                //ffmpegUtils.tailorVideo();

            }
            else
                new ResponseEntity("No recorded files found for the given start " +
                        timestamp + " and duratiojn " + len + " for the camera " + sn, HttpStatus.NOT_FOUND);
        }else{
            new ResponseEntity("No records found for the given start " +
                    timestamp + " and duratiojn "+ len + " for the camera " + sn, HttpStatus.NOT_FOUND);
        }

        long now = System.currentTimeMillis();
        int wait = Long.valueOf((timestamp + len*1000 + 200000 - now)/1000L).intValue();
        if(timestamp + len*1000 + 200000 >= now){
            return new ResponseEntity("the video is being saved, according to the request range,  please wait at least for " + wait + " seconds, in order to complete the video saving process " + sn, HttpStatus.OK);
        }

        return new ResponseEntity("the video is being saved, please wait for a moment in order to complete the video saving progress " + sn, HttpStatus.OK);
    }


    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff/1000).intValue();
    }

    private boolean isFileInScope(long created, long timestamp,long lastModify){
        if((created <= timestamp && timestamp <= lastModify) || timestamp<=created){
            log.info("file last modify >>> " + lastModify);
            log.info("file created >>> " + created);
            log.info("request start time >>> " + timestamp);
            return true;
        }
        else
            return false;
    }


    //1581660766000
    //1581660826000
    //
    //
    //1581660802220
    //1581661002220

    private boolean isFileInScopeV2(long created, long timestamp, long requiredEnd, long lastModify){
//        if((created<=timestamp&&timestamp<=lastModify)
//                || (created<=requiredEnd && requiredEnd<=lastModify)
//                || (timestamp<=created && lastModify <= requiredEnd)
//        )
//            return true;

        if( (timestamp<=created && requiredEnd >=created && requiredEnd <= lastModify)||
                (timestamp >= created && requiredEnd<=lastModify) ||
                (timestamp>=created && timestamp<=lastModify && requiredEnd >=lastModify)||
                (timestamp<=created && requiredEnd >= lastModify)
        )
            return true;
        else
            return false;

    }

}
