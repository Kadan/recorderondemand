package com.ilabservice.darwinrecorder.hlslive.controller;

import ch.qos.logback.core.util.FileUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.io.Files;
import com.ilabservice.darwinrecorder.hlslive.config.InfluxDbUtils;
import com.ilabservice.darwinrecorder.hlslive.config.MQTTConfiguration;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.ffmpeg.FFMPEGUtils;
import com.ilabservice.darwinrecorder.hlslive.measurements.RecorderMeasurement;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import com.ilabservice.darwinrecorder.hlslive.util.Handler;
import com.ilabservice.darwinrecorder.hlslive.util.RecorderRedisHandler;
import org.apache.commons.lang3.StringUtils;
import org.influxdb.dto.Point;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.FileCopyUtils;

import javax.persistence.Lob;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Component
public class RecorderCorn {


    @Autowired
    VideoService videoService;

    @Value("${mqtt.producer.sendMessage}")
    private boolean sendMsg;

    @Autowired
    RecorderRedisHandler recorderRedisHandler;

    @Value("${mqtt.producer.default-topic}")
    private String defaultTopic;

    @Value("${videoRecordPath}")
    private String videoRecordPath;

    @Value("${fileDownloadPath}")
    private String fileDownloadPath;

    @Value("${videoRecordPath-temp}")
    private String videoRecordPath_temp;

    @Autowired
    InfluxDbUtils influxDbUtils;

    @Autowired
    Handler handler;

    @Autowired
    Map<String,Long> recoerderFileSizeChecking;

    @Autowired
    Map<String,String> recoerderFileNameChecking;

    @Autowired
    FFMPEGUtils ffmpegUtils;

    @Autowired
    private MQTTConfiguration.MqttGateway mqttGateway;

    private static ThreadFactory rawRecordFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### recorder corn thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rawExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rawRecordFactory);


    private static ThreadFactory rtspDaemonFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### rtspDaemonFactory corn thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rtspDaemonExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rtspDaemonFactory);


    private static ThreadFactory rtmpDaemonFactory = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger();

        @Override
        public Thread newThread(Runnable runable) {
            return new Thread(runable, "### rtmpDaemonFactory corn thread ### " + counter.getAndIncrement());
        }
    };

    private static ThreadPoolExecutor rtmpDaemonExecutor = new ThreadPoolExecutor(300, 2000, 3, TimeUnit.MINUTES, new ArrayBlockingQueue<Runnable>(100), rtmpDaemonFactory);




    private static final Logger log = LoggerFactory.getLogger(RecorderCorn.class);

    @Scheduled(initialDelay = 120000,fixedRate = 3600000)
    public void recorderBaker(){
        Map<String, Long> requestMap = recorderRedisHandler.getRecorderMap();

        List<String> recordingList = requestMap.keySet().stream().collect(Collectors.toList());

        if(recordingList != null && recordingList.size() > 0){

            recordingList.parallelStream().forEach(stream->{
                        rawExecutor.submit(
                                new Callable<Integer>() {
                                    @Override
                                    public Integer call() {
                                        handle(stream,requestMap);
                                        return 0;
                                    }
                                }
                        );
                    }
            );

        }

    }

    private void handle(String stream, Map<String, Long> requestMap){
        Map<String,String> stoppingMap = recorderRedisHandler.getStoppingMap();
        if (!videoRecordPath_temp.endsWith(File.separator))
            videoRecordPath_temp = videoRecordPath_temp + File.separator;

        if (!videoRecordPath.endsWith(File.separator))
            videoRecordPath = videoRecordPath + File.separator;

        DecimalFormat df = new DecimalFormat("#.0");

        log.info("recorder cron baker >>> start to handle the stream " + stream);

        if(stoppingMap.keySet().contains(stream)) {
            log.info("recorder cron baker >>> the stream is already being stopped " + stream);
            return;
        }
        String recordPath_temp = videoRecordPath_temp +
                stream + "-manual" + File.separator;

        String recordPath = videoRecordPath +
                stream + "-manual" + File.separator;

        if (new File(recordPath_temp).exists()){
            List<File> rawreadyFiles = Arrays.stream(new File(recordPath_temp).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                    && s.getName().contains("raw-ready")).collect(Collectors.toList());

            if(rawreadyFiles!=null && rawreadyFiles.size()>0){

                recorderRedisHandler.saveStoppingStatus(stream);

                Collections.sort(rawreadyFiles);
                rawreadyFiles.remove(rawreadyFiles.size()-1); //remove the current in recording file from the queue
                if(rawreadyFiles.size()<=0){
                    recorderRedisHandler.removeStoppingStatus(stream);
                    return;
                }
                String firstFile = rawreadyFiles.get(0).getName();
                Long sTime = requestMap.get(stream);
                String startTime = String.valueOf(sTime);
                Long startTime_First = Long.valueOf(StringUtils.substringBetween(firstFile,"raw-ready",".mp4"));

                String endTime = String.valueOf(rawreadyFiles.get(rawreadyFiles.size()-1).lastModified());
                recorderRedisHandler.saveRecorderStatus(stream,Long.valueOf(endTime));

                String downloadPath = fileDownloadPath.endsWith(File.separator) ? fileDownloadPath + stream + "-manual" + File.separator + startTime + "-" + endTime + File.separator
                        : fileDownloadPath + File.separator + stream + "-manual" + File.separator + startTime + "-" + endTime + File.separator;

                String massageFolder = recordPath + startTime + "-" + endTime + File.separator;

                if(!new File(massageFolder).exists())
                    new File(massageFolder).mkdirs();

                rawreadyFiles.stream().forEach(s->{
                    log.info("copying file " + s.getName() + " to " + massageFolder + File.separator);
                    try {
                        Files.copy(s, new File(massageFolder + File.separator + s.getName()));
                    } catch (IOException e) {
                        log.info("failed to copy file " + s.getName() );
                        e.printStackTrace();
                    }
                });

                rawreadyFiles.stream().forEach(s->{
                    log.info("removing the file from temp folder " + s.getName() );
                    s.delete();
                });

                recorderRedisHandler.removeStoppingStatus(stream);

                if (!new File(massageFolder).exists()) {
                    return;
                } else {
                    try {
                        PrintWriter out = new PrintWriter(new BufferedWriter(
                                new FileWriter(massageFolder + "index.txt", true)));

                        List<File> rrFiles = Arrays.stream(new File(massageFolder).listFiles()).filter(s -> s != null && s.isFile() && s.getName().endsWith(".mp4")
                                && s.getName().contains("raw-ready")).collect(Collectors.toList());
                        Collections.sort(rrFiles);

                        boolean indexing = false;
                        List<String> vaildFiles = new ArrayList<String>();

                        if (rrFiles != null && rrFiles.size() > 0) {
                            for (int i = 0; i < rrFiles.size(); i++) {
                                String fName = rrFiles.get(i).getName();
                                if (ffmpegUtils.validVideo(massageFolder + fName) && ffmpegUtils.getVideoTime(massageFolder + fName) > 2){//only writes the valid files which is fbs > 50
                                    out.println("file " + fName);
                                    indexing=true;
                                    vaildFiles.add(fName);
                                }
                                else
                                    continue;
                            }

                            if (out != null)
                                out.close();

                            if(!indexing){
                                log.info("could not found any valid file to be concat");
                                return;
                            }
                            int startOffset = 0;
                            if(vaildFiles.size()>0){
                                Collections.sort(vaildFiles);
                                long validFirstStart = Long.valueOf(StringUtils.substringBetween(vaildFiles.get(0),"raw-ready",".mp4"));
                                //long lastModified = new File(massageFolder + vaildFiles.get(vaildFiles.size()-1)).lastModified();
                                if(sTime>validFirstStart)
                                    startOffset = getDuration(validFirstStart,sTime);
                                else
                                    startOffset = 0;
                            }

                            String outputFile_temp = massageFolder + "temp_" + startTime + "-" + endTime + ".mp4";
                            String outputFile = massageFolder + startTime + "-" + endTime + ".mp4";
                            String outputImage = massageFolder + startTime + "-" + endTime + ".jpg";
                            ffmpegUtils.concat(massageFolder + "index.txt", outputFile_temp);
                            //startOffset = getDuration(sTime,startTime_First);
                            int totalDuration = ffmpegUtils.getVideoTime(outputFile_temp);
                            int duration = totalDuration - startOffset;
                            if(duration <=0) duration = totalDuration;
                            log.info("baker >>> tailoring video with startOffset >>> " + startOffset + " and duration >>> " + duration);

                            try{
                                ffmpegUtils.tailorVideo(outputFile_temp,outputFile,startOffset,outputImage,duration);
                            }catch(Exception e){
                                FileCopyUtils.copy(new File(outputFile_temp),new File(outputFile));
                                ffmpegUtils.getFirstFrameAsImage(outputFile,outputImage);
                            }

                            if(new File(outputFile).exists()){
                                Video v = new Video();
                                v.setStartTime(Long.valueOf(startTime));
                                v.setEndTime(Long.valueOf(endTime));
                                v.setCameraId(stream);
                                v.setStatus(Constants.CREATED);
                                v.setVideo(downloadPath + startTime + "-" + endTime + ".mp4");
                                v.setImage(downloadPath + startTime + "-" + endTime + ".jpg");
                                v.setLength(duration * 1L);
                                v.setVaStartTime(System.currentTimeMillis());
                                v.setSize(new File(outputFile).length());
                                videoService.save(v);
                                RecorderMeasurement rm1 = new RecorderMeasurement();
                                rm1.setTime(System.currentTimeMillis());
                                rm1.setEvent("Start to stop record stream");
                                rm1.setType("videoRecorder");
                                rm1.setName(stream);
                                rm1.setRecordingCount(1);
                                rm1.setRecordedHours(Double.valueOf(df.format(totalDuration / 60)));
                                rm1.setRecords(0);
                                Point point1 = Point.measurementByPOJO(rm1.getClass()).addFieldsFromPOJO(rm1).build();
                                influxDbUtils.influxDB.write(point1);
                                if (sendMsg) {
                                    mqttGateway.sendToMqtt(defaultTopic, JSON.toJSONString(v));}
                            }
                        }
                    } catch (Exception e) {
                        log.info("unable to bake the file");
                        //e.printStackTrace();
                    }


                }
            }
        }
    }

    public int getDuration(long startTime, long endTime){
        long diff;
        if (startTime < endTime) {
            diff = endTime - startTime;
        } else {
            diff = 0;
        }

        return Long.valueOf(diff%1000==0?diff/1000:diff/1000+1).intValue();
    }


    @Scheduled(initialDelay = 20000,fixedRate = 10000)
    private void recordDaemon(){
        Map<String,String> rtmpRecorderMap = recorderRedisHandler.getRTMPRecorderMap();
        Map<String,Long> rtspRecorderMap = recorderRedisHandler.getRecorderMap();

        if(!videoRecordPath_temp.endsWith(File.separator))videoRecordPath_temp = videoRecordPath_temp + File.separator;

        rtmpRecorderMap.keySet().stream().filter(s->handler.rtmpRecordInprogress(rtmpRecorderMap.get(s))).forEach(s->{
            rtmpDaemonExecutor.submit(
                    new Callable<Integer>() {
                        @Override
                        public Integer call() {
                            String rtmpUrl = rtmpRecorderMap.get(s);
                            String path = videoRecordPath_temp + s;
                            //log.info("checking rtmpurl's recording status " + rtmpUrl + " in folder " + path);
                            return checkCurrentFileSize(rtmpUrl,path,"rtmp",s);
                            //return 0;
                        }
                    }
            );
        });

        rtspRecorderMap.keySet().stream().filter(r->handler.streamLive(r)).forEach(r->{
            rtspDaemonExecutor.submit(
                    new Callable<Integer>() {
                        @Override
                        public Integer call() {
                            String path = videoRecordPath_temp + r;
                            //log.info("checking rtsp's recording status " + r + " in folder " + path+"-manual");
                            return checkCurrentFileSize(r,path+"-manual","rtsp",r);
                            //return 0;
                        }
                    }
            );
        });
    }



    public int checkCurrentFileSize(String stream,String path,String type,String key){
        File folder = new File(path);
        long preSize = 0;
        if(recoerderFileSizeChecking.keySet().size() ==0) preSize =0;
        else if(recoerderFileSizeChecking.keySet().contains(key))
            preSize = recoerderFileSizeChecking.get(key);
        //log.info("the previous file size is " + preSize);

        if(folder.exists()){
            File[] files = folder.listFiles();
            if(files != null && files.length > 0){
                List<File> currentFiles = Arrays.stream(files).filter(f->
                        StringUtils.endsWith(f.getName(),"mp4")&&
                                StringUtils.containsIgnoreCase(f.getName(),"raw-ready")
                ).collect(Collectors.toList());

                if(currentFiles !=null && currentFiles.size()>0){
                    Collections.sort(currentFiles);
                    File currentFile = currentFiles.get(currentFiles.size()-1);
                    String currentFileName = currentFile.getName();
                    String preFileName = null;
                    if(recoerderFileNameChecking.containsKey(key))
                        preFileName = recoerderFileNameChecking.get(key);
                    //the very first time
                    if(preFileName == null){
                        //log.info("there is no comparision before  ");
                        recoerderFileNameChecking.put(key,currentFileName);
                        return 0;
                    }
                    //file changed
                    if(!StringUtils.equalsIgnoreCase(currentFileName,preFileName)){
                        //log.info("the file has switched  from  " + preFileName + " to " + currentFileName);
                        recoerderFileNameChecking.put(key,currentFileName);
                    }

                    if(StringUtils.equalsIgnoreCase(currentFileName,preFileName)){
                        //log.info("it is still the file  " + currentFileName);
                        Long size = currentFile.length();
                        //log.info("the current file size is " + size);
                        if(preSize <=0){
                            recoerderFileSizeChecking.put(key,size);
                        }else{
                            if(size != preSize){
                                recoerderFileSizeChecking.put(key,size);
                                return 0;
                            }
                            else if(size == preSize){
                                //log.info("The file size stop growing, stop the process  " + stream);
                                if(type.equalsIgnoreCase("rtmp"))
                                    handler.stopRTMPRecord(stream);
                                if(type.equalsIgnoreCase("rtsp"))
                                    handler.Stop(key);
                            }
                        }
                    }else
                        return 0;
                }
            }
        }else
            return 1;

        return 0;

    }
}
