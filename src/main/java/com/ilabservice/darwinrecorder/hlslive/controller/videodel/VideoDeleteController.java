package com.ilabservice.darwinrecorder.hlslive.controller.videodel;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ilabservice.darwinrecorder.hlslive.constants.Constants;
import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.service.EvtVideoService;
import com.ilabservice.darwinrecorder.hlslive.util.FileUtils;
import com.ilabservice.darwinrecorder.hlslive.util.MQTTMessageHandler;
import io.swagger.annotations.*;
import netscape.javascript.JSObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class VideoDeleteController {

    private static final Logger log = LoggerFactory.getLogger(VideoDeleteController.class);
    @Autowired
    EvtVideoService videoService;

    @Value("${fileDownloadPath}")
    private String downloadPath;
    @Value("${videoRecordPath}")
    private String savePath;
    @ApiOperation("移动侦测视频删除")
    @ApiResponses({ @ApiResponse(code = 200, message = "删除成功"),
            @ApiResponse(code = 400, message = "删除失败")})
    @ApiImplicitParams({ @ApiImplicitParam(paramType = "query", dataType = "Boolean", name = "markDelete", value = "true", required = true),
            @ApiImplicitParam(paramType = "body", dataType = "String", name = "cameraSN", value = "D70002092", required = true),
            @ApiImplicitParam(paramType = "body", dataType = "Long", name = "startTime", value = "1576744320000", required = false,allowMultiple=true)
    })
    //@GetMapping("/evtvideos/del")
    @PostMapping("/evtvideos/del")
    public ResponseEntity deleteVideo(HttpServletRequest request,
                                      @RequestParam(value="markDelete", required = true) boolean markDelete) {
        BufferedReader bfr = null;
        StringBuilder sb = new StringBuilder("");
        try {
            bfr = request.getReader();
            String str;
            while ((str = bfr.readLine()) != null) {
                sb.append(str);
            }
            bfr.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != bfr) {
                try {
                    bfr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        String requestJson = sb.toString();
        JSONObject jsonObject = null;
        try {
            jsonObject = JSON.parseObject(requestJson);
        } catch (Exception e) {
            return new ResponseEntity("the request string in the request body is malformat json", HttpStatus.BAD_REQUEST);
        }
        String cameraSN = "";
        if(jsonObject !=null){
            cameraSN = jsonObject.getString("cameraSN");
            log.info("handling camera  " + cameraSN);
            if(StringUtils.isEmpty(cameraSN))
                return new ResponseEntity("Camera SN should not be empty", HttpStatus.BAD_REQUEST);

//            if(!jsonObject.containsKey("startTime"))
//                return new ResponseEntity("please provide an array (startTime)", HttpStatus.BAD_REQUEST);

            JSONArray jsonArray = null;
            if(jsonObject.containsKey("startTime"))
                jsonObject.getJSONArray("startTime");

//            if(jsonArray==null || jsonArray.size()==0)
//                return new ResponseEntity("please provide value(s) for (startTime)", HttpStatus.BAD_REQUEST);

            if(jsonArray==null || jsonArray.size()==0){//delete all the videos belonging to the specific camera
                if(!markDelete){
                    log.info("deleting all the records for   " + cameraSN);
                    List<EvtVideo> foundList = videoService.findByCameraId(cameraSN);
                    if(foundList!=null && foundList.size()>0){
                        List<EvtVideo> validlist = foundList.stream().filter(v->!StringUtils.equalsIgnoreCase(v.getStatus(),Constants.DELETED)).collect(Collectors.toList());
                        if(validlist!=null && validlist.size()>0)
                            videoService.deleteByCameraId(cameraSN);
                        else
                            return new ResponseEntity("No corresponding record found for given camera " + cameraSN, HttpStatus.BAD_REQUEST);
                    }
                    else
                        return new ResponseEntity("No corresponding record found for given camera " + cameraSN, HttpStatus.BAD_REQUEST);

//                    videoService.deleteByCameraId(cameraSN);
                    String tSavePath = "";
                    if(!savePath.endsWith(File.separator))
                        tSavePath = savePath + File.separator;
                    String targetDir = tSavePath + cameraSN;
                    log.info("deleting all the files under " + targetDir);
                    new FileUtils().delFile(new File(targetDir));
                    return new ResponseEntity("Requested Videos have been deleted", HttpStatus.OK);
                }else{
                    List<EvtVideo> videoList = videoService.findByCameraId(cameraSN);
                    if(videoList!=null){
                        List<EvtVideo> tobeUpdatedList = videoList.stream().filter(v->!StringUtils.equalsIgnoreCase(v.getStatus(),Constants.DELETED)).map(v->{
                            v.setStatus(Constants.DELETED);
                            return v;
                        }).collect(Collectors.toList());
                        if(tobeUpdatedList!=null && tobeUpdatedList.size() >0){
                            log.info("mark all the records with cameraid  " + cameraSN + " as DELETED TOTAL " + tobeUpdatedList.size());
                            videoService.save(tobeUpdatedList);
                        }
                    }
                    return new ResponseEntity("Requested Videos have been deleted", HttpStatus.OK);
                }
            }


            for(int i=0;i<jsonArray.size();i++){
                Long startTime = (Long) jsonArray.get(i);
                if(!markDelete){
                    log.info("phsically delete the video record and files");
                    EvtVideo video = videoService.findByCameraIdAndStartTime(cameraSN,startTime);
                    if(video!=null && video.getStatus() !=null && video.getStatus().equalsIgnoreCase(Constants.DELETED)){
                        log.info("the record has been marked deleted");
                        continue;
                    }
                    if(video != null){
                        String v = video.getVideo();
                        String tDownloadpath = "";
                        if(!downloadPath.endsWith(File.separator))
                            tDownloadpath = downloadPath+File.separator;
                        else
                            tDownloadpath = downloadPath;
                        String tSavePath = "";
                        if(!savePath.endsWith(File.separator))
                            tSavePath = savePath + File.separator;
                        else
                            tSavePath = savePath;
                        String tardir = StringUtils.replace(v,tDownloadpath,tSavePath);
                        tardir = StringUtils.substringBeforeLast(tardir,File.separator);

                        log.info("deleting all the files under " + tardir);
                        new FileUtils().delFile(new File(tardir));
//                        try{
//                        if(new File(tardir).exists()){
//                            if(new File(tardir).isFile())
//                                new File(tardir).delete();
//                            else for(File f:new File(tardir).listFiles())
//                                f.delete();
//                            new File(tardir).delete();
//                        }}catch(Exception e){log.info(e.getMessage());}//should not happen
                        videoService.deleteByCameraIdAndStartTime(cameraSN,startTime);
                    }else{
                        log.info("No videos found with the camera" + cameraSN + " and the startTime " + startTime);
                        continue;
                    }
                }
                else{
                    log.info("mark the record as deleted");
                    EvtVideo video = videoService.findByCameraIdAndStartTime(cameraSN,startTime);
                    if(video != null){
                        if(video.getStatus() !=null && video.getStatus().equalsIgnoreCase(Constants.DELETED)){
                            log.info("the record has already been marked deleted");
                            continue;
                        }
                        video.setStatus(Constants.DELETED);
                        videoService.save(video);
                    }
                    else{
                        log.info("No videos found with the camera" + cameraSN + " and the startTime " + startTime);
                        continue;
                    }

                }
            }


        }

        return new ResponseEntity("Requested Videos have been deleted", HttpStatus.OK);
    }
}
