package com.ilabservice.darwinrecorder.hlslive.controller;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

import java.util.concurrent.TimeUnit;

public class Cutting {

//    public static void main(String[] args){
//
//        new Cutting().recordBuilder("/Users/lijunjie/KadanLab/data/raw.mp4","/Users/lijunjie/KadanLab/data/raw_cut.mp4");
//
//    }

    public void recordBuilder(String inputRTSP, String outputFile) {
        try {
            FFmpegBuilder builder1 =
                    new FFmpegBuilder()
                            .setInput(inputRTSP)
                            .overrideOutputFiles(false)
                            .addOutput(outputFile)
                            .setDuration(10, TimeUnit.SECONDS)
                            .setVideoCodec("copy")
                            .setAudioCodec("copy")
                            .setAudioChannels(1)
                            .setAudioBitStreamFilter("aac_adtstoasc")
                            .setFormat("mp4")
                            .done();
            new FFmpegExecutor(new FFmpeg("/usr/local/bin/ffmpeg"), new FFprobe("/usr/local/bin/ffprobe")).createJob(builder1).run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
