package com.ilabservice.darwinrecorder.hlslive.service.impl;

import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;
import com.ilabservice.darwinrecorder.hlslive.repository.EvtVideoRepository;
import com.ilabservice.darwinrecorder.hlslive.repository.VideoRepository;
import com.ilabservice.darwinrecorder.hlslive.service.EvtVideoService;
import com.ilabservice.darwinrecorder.hlslive.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EvtVideoServiceImpl implements EvtVideoService {
    @Autowired
    EvtVideoRepository videoRepository;

    @Override
    public List<EvtVideo> findByVideoName(String videoName) {
        return videoRepository.findByVideo(videoName);
    }

    @Override
        public void save(EvtVideo video) {
        videoRepository.save(video);
        }

    @Override
    public void save(List<EvtVideo> evtVideoList) {
        videoRepository.saveAll(evtVideoList);
    }

    @Override
    public Map<String,EvtVideo> findByVideoNameIn(List<String> videoNames) {
        Map<String,EvtVideo> retMap = new HashMap<String,EvtVideo>();

        List<EvtVideo> videos = videoRepository.findByVideoIn(videoNames);
        if(videos!=null){
            videos.stream().forEach(s->retMap.put(s.getVideo(),s));
        }
        return retMap;
    }

    @Override
    public List<EvtVideo> findByStartTimeAndEndTime(long startTime, long endTime) {
        List<EvtVideo> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThan(endTime,startTime);
        return videoList;
    }

    @Override
    public List<EvtVideo> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId) {
        List<EvtVideo> videoList = videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraId(endTime,startTime,cameraId);
        return videoList;
    }

    @Override
    @Transactional
    public void deleteByCameraIdAndStartTime(String cameraId, long startTime) {
        videoRepository.deleteByCameraIdAndStartTime(cameraId,startTime);
    }

    @Override
    @Transactional
    public void deleteByCameraId(String cameraId) {
        videoRepository.deleteByCameraId(cameraId);
    }

    @Override
    public EvtVideo findByCameraIdAndStartTime(String cameraId, long startTime) {
        List<EvtVideo> evtVideoList = videoRepository.findByCameraIdAndStartTime(cameraId,startTime);
        if(evtVideoList!=null && evtVideoList.size()>0)
            return evtVideoList.get(0);
        else
            return null;

    }

    @Override
    public List<EvtVideo> findByCameraId(String cameraId) {
        return videoRepository.findByCameraId(cameraId);
    }

    @Override
    public EvtVideo findByImage(String image) {
        List<EvtVideo> evtVideoList = videoRepository.findByImage(image);
        if(evtVideoList.size()>0)
            return evtVideoList.get(0);
        else
            return null;
    }

    @Override
    public List<EvtVideo> findByStartTimeAndEndTimeAndCameraIdAndWithFeature(long startTime, long endTime, String cameraId) {
        return videoRepository.findByStartTimeLessThanEqualAndEndTimeGreaterThanAndCameraIdAndVaEndTimeGreaterThanAndVaCode(endTime,startTime,cameraId,0L,"0");
    }

    @Override
    public List<EvtVideo> findByStartTimeAndCameraIdAndVaresultContaining(long startTime, long endTime, String cameraId, String varesult) {
        return videoRepository.findByCameraIdAndStartTimeAndVaresultContaining(cameraId,startTime,varesult);
    }

    @Override
    public List<EvtVideo> findByStartTimeAndEndTimeAndCameraIdAndVaresultContaining(long startTime, long endTime, String cameraId, String varesult) {
        return videoRepository.findByCameraIdAndStartTimeAndEndTimeAndVaresultContaining(cameraId,startTime,endTime,varesult);
    }

}
