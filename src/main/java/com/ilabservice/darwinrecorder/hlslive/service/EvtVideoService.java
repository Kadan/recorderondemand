package com.ilabservice.darwinrecorder.hlslive.service;

import com.ilabservice.darwinrecorder.hlslive.entity.EvtVideo;
import com.ilabservice.darwinrecorder.hlslive.entity.Video;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

public interface EvtVideoService {

    List<EvtVideo> findByVideoName(String videoName);

    void save(EvtVideo video);

    void save(List<EvtVideo> evtVideoList);

    Map<String,EvtVideo> findByVideoNameIn(List<String> videoNames);

    List<EvtVideo> findByStartTimeAndEndTime(long startTime, long endTime);

    List<EvtVideo> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime, String cameraId);

    @Transactional
    void deleteByCameraIdAndStartTime(String cameraId,long startTime);

    @Transactional
    void deleteByCameraId(String cameraId);

    EvtVideo findByCameraIdAndStartTime(String cameraId,long startTime);

    List<EvtVideo> findByCameraId(String cameraId);

    EvtVideo findByImage(String image);

    List<EvtVideo> findByStartTimeAndEndTimeAndCameraIdAndWithFeature(long startTime, long endTime, String cameraId);
    List<EvtVideo> findByStartTimeAndCameraIdAndVaresultContaining(long startTime, long endTime, String cameraId,String varesult);
    List<EvtVideo> findByStartTimeAndEndTimeAndCameraIdAndVaresultContaining(long startTime, long endTime, String cameraId,String varesult);

}
