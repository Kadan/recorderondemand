package com.ilabservice.darwinrecorder.hlslive.service;

import com.ilabservice.darwinrecorder.hlslive.entity.Video;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

public interface VideoService {

    List<Video> findByVideoName(String videoName);

    void save(Video video);

    void save(List<Video> videoList);

    Map<String,Video> findByVideoNameIn(List<String> videoNames);

    List<Video> findByStartTimeAndEndTime(long startTime,long endTime);

    List<Video> findByStartTimeAndEndTimeAndCameraId(long startTime, long endTime,String cameraId);

    @Transactional
    void deleteByCameraIdAndStartTime(String cameraId,long startTime);

    @Transactional
    void deleteByCameraId(String cameraId);

    Video findByCameraIdAndStartTime(String cameraId,long startTime);

    List<Video> findByCameraId(String cameraId);

}
