package com.ilabservice.darwinrecorder.hlslive.util;

import java.io.File;

public class FileUtils {

    public boolean delFile(File file){
        if (!file.exists()) {
            return false;
        }
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                delFile(f);
            }
        }
        return file.delete();
    }
}
